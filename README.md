# HashTag Plugin for NodeBB

[FUNZIONALITA']

Permette la gestione degli hashtags all'interno di un topic. In particolare:

- è possibile inserire degli "hashtag" attraverso il "#" in stile Twitter
- è possibile specificare nella pagina dell'amministrazione il numero massimo di hastags che possono essere inseriti all'interno di un post.

<br /><br />
Inserimento numero hashtags massimo nella pagina dell'amministrazione:<br /><br />

![alt text](https://gitlab.com/gt-connect/nodebb-plugin-connect-hashtag/raw/master/screenshot/plugin-hashtags.png)

<br /><br />Inserimento hashtags in un post:<br /><br />

![alt text](https://gitlab.com/gt-connect/nodebb-plugin-connect-hashtag/raw/master/screenshot/plugin-hashtags-scrittura.png)

<br /><br />Visualizzazione topic contenenti l'hashtags:<br /><br />

![alt text](https://gitlab.com/gt-connect/nodebb-plugin-connect-hashtag/raw/master/screenshot/plugin-hashtags-visualizzazione.png)

[TEST]<br />
Per testare il plugin bisogna:<br />

1. andare in admin->plugins->HashTag(GT) e inserire il numero massimo di hashtags per posts<br />
2. scrivere un post e creare i vari hashtags. Per verificare il corretto funzionamento del plugin bisogna creare dei post particolari per esempio:

- bisogna verificare che il numero di hashtags all'interno del post sia minore uguale al numero massimo di hashtags inserito da admin precedentemente.
- bisogna verificare che i "#" all'interno dei link (https://github.com/NodeBB-Community/nodebb-plugin-shortcuts#readme) non vengano interpretati come hastags.
- bisogna verificare che se ci stanno hashtags che sono sotto-hashtags (sotto-hashtag inteso come sotto-stringa) di altri hashtags, essi vengono considerati come hashtags separati.

Per esempio,sapendo che il numero massimo di hashtags in un post è 3, un post che verifica tutte le condizioni potrebbe essere:

"Ciaooo Mondo #ciaoo#ciao https://github.com/NodeBB-Community/nodebb-plugin-shortcuts#readme"

# To Do:

First Init Script
