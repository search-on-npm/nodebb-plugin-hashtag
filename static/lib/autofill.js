"use strict";
/* globals socket, app, utils */

$(document).ready(function () {
  let slugify;
  $(window).on("composer:autocomplete:init", function (ev, data) {
    const strategy = {
      match: /\B#([^\s\n]*)?$/,
      search: function (term, callback) {
        require(["slugify"], function (_slugify) {
          slugify = _slugify;
          var tags = [];
          if (!term) {
            return callback([]);
          }

          socket.emit(
            "topics.searchAndLoadTags",
            { query: term },
            function (err, tagdata) {
              if (err) {
                return callback([]);
              }

              if (tagdata && tagdata.tags) {
                tags = tagdata.tags.map(function (tag) {
                  return tag.value;
                });
              } else {
                callback(null);
              }

              callback(tags);
            }
          );
        });
      },
      index: 1,
      replace: function (hashtag) {
        return "#" + slugify(hashtag, true) + " ";
      },
      cache: true,
    };

    data.strategies.push(strategy);
  });
  $(window).on("action:composer.loaded", function (ev, data) {
    const composer = $("#cmp-uuid-" + data.post_uuid + " .write");
    composer.attr("data-hashtags", "1");

    $(".tags-container").hide();
  });
});
