"use strict";

var Controllers = {};

Controllers.renderAdminPage = async (req, res) => {
  res.render("admin/plugins/connect-hashtag");
};

module.exports = Controllers;
